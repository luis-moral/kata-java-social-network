package social;

public class SocialNetworkApi {

    private final CommandFactory commandFactory;

    public SocialNetworkApi(CommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    public void executeCommand(String command) {
        commandFactory.create(command).execute();
    }
}
