package social;

public class SystemOutput implements Output {

    @Override
    public void printLine(String line) {
        System.out.println(line);
    }
}
