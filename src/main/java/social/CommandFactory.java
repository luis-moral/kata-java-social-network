package social;

public class CommandFactory {

    private static final String COMMAND_WALL = "wall";
    private static final String COMMAND_FOLLOWS = "follows";
    private static final String COMMAND_POST_MESSAGE = "->";

    private final UserRepository userRepository;
    private final MessageRepository messageRepository;
    private final Output output;
    private final MessageFormatter messageFormatter;
    private Clock clock;

    public CommandFactory(UserRepository userRepository, MessageRepository messageRepository, Output output, MessageFormatter messageFormatter, Clock clock) {
        this.userRepository = userRepository;
        this.messageRepository = messageRepository;
        this.output = output;
        this.messageFormatter = messageFormatter;
        this.clock = clock;
    }

    public Command create(String line) {
        ParsedLine parsedLine = parseLine(line);

        if (parsedLine.getCommand() == null) {
            return new ReadMessagesCommand(messageFormatter, messageRepository, output, clock, parsedLine.getUser());
        }

        if (parsedLine.getCommand().equals(COMMAND_WALL)) {
            return new ViewUserWallCommand(userRepository, messageRepository, messageFormatter, output, clock, parsedLine.getUser());
        }

        if (parsedLine.getCommand().equals(COMMAND_FOLLOWS)) {
            return new FollowUserCommand(userRepository, output, parsedLine.getUser(), parsedLine.getTarget());
        }

        if (parsedLine.getCommand().equals(COMMAND_POST_MESSAGE)) {
            return new PostMessageCommand(parsedLine.getUser(), parsedLine.getTarget(), userRepository, messageRepository);
        }

        return new ErrorParsingCommand(output);
    }

    private ParsedLine parseLine(String line) {
        String[] split = line.split(" ", 3);

        return
            new ParsedLine(
                split[0],
                split.length > 1 ? split[1] : null,
                split.length > 2 ? split[2] : null
            );
    }

    private class ParsedLine {
        private final String user;
        private final String command;
        private final String target;

        private ParsedLine(String user, String command, String target) {
            this.user = user;
            this.command = command;
            this.target = target;
        }

        public String getUser() {
            return user;
        }

        public String getCommand() {
            return command;
        }

        public String getTarget() {
            return target;
        }
    }
}
