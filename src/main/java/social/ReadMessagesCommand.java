package social;

import java.util.Objects;

public class ReadMessagesCommand implements Command {

    private final MessageFormatter messageFormatter;
    private final MessageRepository messageRepository;
    private final Output output;
    private final Clock clock;
    private final String user;

    public ReadMessagesCommand(MessageFormatter messageFormatter, MessageRepository messageRepository, Output output, Clock clock, String user) {
        this.messageFormatter = messageFormatter;
        this.messageRepository = messageRepository;
        this.output = output;
        this.clock = clock;
        this.user = user;
    }

    public void execute() {
        try {
            messageRepository.getMessagesForUser(user)
                .stream()
                .forEach(message ->
                        output.printLine(messageFormatter.formatForRead(message, clock.now()))
                );
        }
        catch (UserNotFoundException e) {
            output.printLine(e.getLocalizedMessage());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReadMessagesCommand that = (ReadMessagesCommand) o;
        return Objects.equals(user, that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user);
    }
}
