package social;

public interface Output {

    void printLine(String line);
}
