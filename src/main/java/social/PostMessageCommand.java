package social;

import java.util.Objects;

public class PostMessageCommand implements Command {

    private final String user;
    private final String message;
    private final UserRepository userRepository;
    private final MessageRepository messageRepository;

    public PostMessageCommand(String user, String message, UserRepository userRepository, MessageRepository messageRepository) {
        this.user = user;
        this.message = message;
        this.userRepository = userRepository;
        this.messageRepository = messageRepository;
    }

    @Override
    public void execute() {
        userRepository.create(user);
        messageRepository.addMessageForUser(user, message);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PostMessageCommand that = (PostMessageCommand) o;
        return Objects.equals(user, that.user) &&
                Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, message);
    }
}
