package social;

import javafx.util.Pair;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Objects;
import java.util.stream.Collectors;

public class ViewUserWallCommand implements Command {

    private final String user;
    private final UserRepository userRepository;
    private final MessageRepository messageRepository;
    private final MessageFormatter messageFormatter;
    private final Output output;
    private final Clock clock;

    public ViewUserWallCommand(UserRepository userRepository, MessageRepository messageRepository, MessageFormatter messageFormatter, Output output, Clock clock, String user) {
        this.userRepository = userRepository;
        this.messageRepository = messageRepository;
        this.messageFormatter = messageFormatter;
        this.output = output;
        this.clock = clock;
        this.user = user;
    }

    public void execute() {
        try {
            getUsersInTimeline()
                .stream()
                .map(someUser -> new Pair<>(someUser, messageRepository.getMessagesForUser(someUser)))
                .flatMap(pair ->
                    pair
                        .getValue()
                        .stream()
                        .map(message -> new Pair<>(pair.getKey(), message))
                )
                .sorted((o1, o2) -> Long.compare(o2.getValue().getTime(), o1.getValue().getTime()))
                .forEach(messagePair -> output.printLine(messageFormatter.formatForTimeline(messagePair.getKey(), messagePair.getValue(), clock.now())));
        }
        catch (UserNotFoundException e) {
            output.printLine(e.getLocalizedMessage());
        }
    }

    private Collection<String> getUsersInTimeline() {
        Collection<String> usersInTimeline = new LinkedList<>(userRepository.getUsersFollowedBy(user));
        usersInTimeline.add(user);

        return usersInTimeline;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ViewUserWallCommand that = (ViewUserWallCommand) o;
        return Objects.equals(user, that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user);
    }
}
