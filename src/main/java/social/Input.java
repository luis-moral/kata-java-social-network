package social;

public class Input {

    private final SocialNetworkApi socialNetworkApi;

    public Input(SocialNetworkApi socialNetworkApi) {
        this.socialNetworkApi = socialNetworkApi;
    }

    public void readLine(String line) {
        socialNetworkApi.executeCommand(line);
    }
}
