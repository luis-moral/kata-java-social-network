package social;

import java.util.*;

public class MessageRepository {

    private final Clock clock;
    private final Map<String, Set<Message>> messageMap;

    public MessageRepository(Clock clock) {
        this.clock = clock;

        messageMap = new HashMap<>();
    }

    public void addMessageForUser(String user, String message) {
        Set<Message> messages = messageMap.get(user);

        if (messages == null) {
            messages = new TreeSet<>(
                (o1, o2) -> Long.compare(o2.getTime(), o1.getTime())
            );
            messageMap.put(user, messages);
        }

        messages.add(new Message(message, clock.now()));
    }

    public Collection<Message> getMessagesForUser(String user) {
        Collection<Message> messages = messageMap.get(user);

        if (messages == null) {
            throw new UserNotFoundException(user);
        }

        return messages;
    }
}
