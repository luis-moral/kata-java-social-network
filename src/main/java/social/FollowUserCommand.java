package social;

import java.util.Objects;

public class FollowUserCommand implements Command {

    private final UserRepository userRepository;
    private final Output output;
    private final String user;
    private final String userToFollow;

    public FollowUserCommand(UserRepository userRepository, Output output, String user, String userToFollow) {
        this.userRepository = userRepository;
        this.output = output;
        this.user = user;
        this.userToFollow = userToFollow;
    }

    public void execute() {
        try {
            userRepository.followUser(user, userToFollow);
        }
        catch (UserNotFoundException e) {
            output.printLine(e.getLocalizedMessage());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FollowUserCommand that = (FollowUserCommand) o;
        return Objects.equals(user, that.user) &&
                Objects.equals(userToFollow, that.userToFollow);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, userToFollow);
    }
}
