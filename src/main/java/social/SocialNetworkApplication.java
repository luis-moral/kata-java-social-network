package social;

import java.util.Scanner;

public class SocialNetworkApplication {

    public static void main(String[] args) {
        SystemClock systemClock = new SystemClock();
        SystemOutput systemOutput = new SystemOutput();

        Input input =
            new Input(
                new SocialNetworkApi(
                    new CommandFactory(
                            new UserRepository(),
                            new MessageRepository(systemClock),
                            systemOutput,
                            new MessageFormatter(),
                            systemClock
                    )
                )
            );

        systemOutput.printLine("Welcome to Social Network:\n");

        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter("\n");

        while(scanner.hasNextLine()){
            input.readLine(scanner.nextLine());
        }

        scanner.close();
    }
}
