package social;

public interface Clock {
    long now();
}
