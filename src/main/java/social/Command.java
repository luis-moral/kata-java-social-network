package social;

public interface Command {

    void execute();
}
