package social;

import java.util.Objects;

public class ErrorParsingCommand implements Command {

    private static final String MESSAGE_WRONG_COMMAND = "Wrong command.";

    private final Output output;

    public ErrorParsingCommand(Output output) {
        this.output = output;
    }

    @Override
    public void execute() {
        output.printLine(MESSAGE_WRONG_COMMAND);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ErrorParsingCommand that = (ErrorParsingCommand) o;
        return Objects.equals(output, that.output);
    }

    @Override
    public int hashCode() {
        return Objects.hash(output);
    }
}
