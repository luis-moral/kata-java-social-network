package social;

import java.util.*;

public class UserRepository {

    private final Set<String> users;
    private final Map<String, Collection<String>> usersFollowedMap;

    public UserRepository() {
        users = new HashSet<>();
        usersFollowedMap = new HashMap<>();
    }

    public void create(String user) {
        users.add(user);
        usersFollowedMap.putIfAbsent(user, new HashSet<>());
    }

    public String findById(String user) {
        if (!users.contains(user)) {
            throw new UserNotFoundException(user);
        }

        return user;
    }

    public void followUser(String user, String userToFollow) {
        if (!users.contains(user)) {
            throw new UserNotFoundException(user);
        }

        if (!users.contains(userToFollow)) {
            throw new UserNotFoundException(userToFollow);
        }

        usersFollowedMap.get(user).add(userToFollow);
    }

    public Collection<String> getUsersFollowedBy(String user) {
        return usersFollowedMap.get(findById(user));
    }
}
