package social;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.concurrent.TimeUnit;

@RunWith(MockitoJUnitRunner.class)
public class SocialNetworkFeature {

    private static final long NOW = System.currentTimeMillis();

    @Spy
    Output output = new SystemOutput();
    @Mock
    Clock repositoryClock;
    @Mock
    Clock factoryClock;

    private UserRepository userRepository;
    private MessageRepository messageRepository;
    private MessageFormatter messageFormatter;
    private Input input;

    @Before
    public void setUp() {
        userRepository = new UserRepository();
        messageRepository = new MessageRepository(repositoryClock);
        messageFormatter = new MessageFormatter();
        input = new Input(new SocialNetworkApi(new CommandFactory(userRepository, messageRepository, output, messageFormatter, factoryClock)));
    }

    @Test public void
    user_can_post_read_follow_and_see_wall_of_other_users() {
        BDDMockito
            .given(repositoryClock.now())
            .willReturn(
                // Post Alice message
                NOW - TimeUnit.MINUTES.toMillis(5),

                // Post Bob messages
                NOW - TimeUnit.MINUTES.toMillis(2),
                NOW - TimeUnit.MINUTES.toMillis(1),

                // Post Charlie message
                NOW - TimeUnit.SECONDS.toMillis(2)
            );

        BDDMockito
            .given(factoryClock.now())
            .willAnswer((Answer<Long>) invocation ->
                Mockito.mockingDetails(invocation.getMock()).getInvocations().size() != 6
                ? NOW
                // Second Charlie wall (after 13 seconds)
                : NOW + TimeUnit.SECONDS.toMillis(13)
            );

        input.readLine("Alice -> I love the weather today");
        input.readLine("Bob -> Damn! We lost!");
        input.readLine("Bob -> Good game though.");
        input.readLine("Alice");
        input.readLine("Bob");
        input.readLine("Charlie -> I'm in New York today! Anyone want to have a coffee?");
        input.readLine("Charlie follows Alice");
        input.readLine("Charlie wall");
        input.readLine("Charlie follows Bob");
        input.readLine("Charlie wall");

        InOrder outputVerifier = Mockito.inOrder(output);
        verifyOutput(outputVerifier, output, "I love the weather today (5 minutes ago)");
        verifyOutput(outputVerifier, output, "Good game though. (1 minute ago)");
        verifyOutput(outputVerifier, output, "Damn! We lost! (2 minutes ago)");
        verifyOutput(outputVerifier, output, "Charlie - I'm in New York today! Anyone want to have a coffee? (2 seconds ago)");
        verifyOutput(outputVerifier, output, "Alice - I love the weather today (5 minutes ago)");
        verifyOutput(outputVerifier, output, "Charlie - I'm in New York today! Anyone want to have a coffee? (15 seconds ago)");
        verifyOutput(outputVerifier, output, "Bob - Good game though. (1 minute ago)");
        verifyOutput(outputVerifier, output, "Bob - Damn! We lost! (2 minutes ago)");
        verifyOutput(outputVerifier, output, "Alice - I love the weather today (5 minutes ago)");
    }

    private void verifyOutput(InOrder outputVerifier, Output output, String line) {
        outputVerifier
            .verify(output, Mockito.times(1))
            .printLine(line);
    }
}
