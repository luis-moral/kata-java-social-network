package social;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

public class UserRepositoryShould {

    private static final String ALICE = "Alice";
    private static final String BOB = "Bob";
    private static final String CHARLIE = "Charlie";

    private UserRepository userRepository;

    @Before
    public void setUp() {
        userRepository = new UserRepository();
    }

    @Test public void
    create_new_users() {
        userRepository.create(ALICE);

        Assertions.assertThat(userRepository.findById(ALICE)).isEqualTo("Alice");
        Assertions.assertThat(userRepository.getUsersFollowedBy(ALICE)).hasSize(0);
    }

    @Test(expected = UserNotFoundException.class) public void
    throw_exception_when_user_doest_not_exist() {
        userRepository.findById(BOB);
    }

    @Test public void
    add_a_user_to_the_users_followed_by_a_user() {
        userRepository.create(ALICE);
        userRepository.create(BOB);
        userRepository.create(CHARLIE);

        userRepository.followUser(ALICE, BOB);
        userRepository.followUser(ALICE, CHARLIE);

        Assertions.assertThat(userRepository.getUsersFollowedBy(ALICE)).containsOnly(BOB, CHARLIE);
    }
}
