package social;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

@RunWith(MockitoJUnitRunner.class)
public class ReadMessagesCommandShould {

    private static final String ALICE = "Alice";
    private static final String ALICE_MESSAGE = "I love the weather today";
    private static final String ALICE_MESSAGE_FORMATTED = ALICE_MESSAGE + " (0 seconds ago)";
    private static final long NOW = System.currentTimeMillis();

    @Mock
    MessageFormatter messageFormatter;
    @Mock
    MessageRepository messageRepository;
    @Mock
    Output output;
    @Mock
    Clock clock;

    @Test public void 
    output_the_user_messages() {
        Message aliceMessage = new Message(ALICE_MESSAGE, NOW);

        BDDMockito
            .given(clock.now())
            .willReturn(NOW);

        BDDMockito
            .given(messageFormatter.formatForRead(aliceMessage, clock.now()))
            .willReturn(ALICE_MESSAGE_FORMATTED);

        BDDMockito
            .given(messageRepository.getMessagesForUser(ALICE))
            .willReturn(Arrays.asList(aliceMessage));

        new ReadMessagesCommand(messageFormatter, messageRepository, output, clock, ALICE).execute();

        Mockito
            .verify(messageFormatter, Mockito.times(1))
            .formatForRead(aliceMessage, clock.now());

        Mockito
            .verify(output, Mockito.times(1))
            .printLine(ALICE_MESSAGE_FORMATTED);
    }

    @Test public void
    output_user_not_found_message_if_the_user_does_not_exists() {
        BDDMockito
            .willThrow(new UserNotFoundException(ALICE))
            .given(messageRepository)
            .getMessagesForUser(ALICE);

        new ReadMessagesCommand(messageFormatter, messageRepository, output, clock, ALICE).execute();

        Mockito
            .verify(output, Mockito.times(1))
            .printLine("User not found: Alice");
    }
}
