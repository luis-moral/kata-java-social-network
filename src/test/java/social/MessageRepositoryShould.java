package social;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.TimeUnit;

@RunWith(MockitoJUnitRunner.class)
public class MessageRepositoryShould {

    private static final String BOB = "Bob";
    private static final String BOB_FIRST_MESSAGE = "Damn! We lost!";
    private static final String BOB_SECOND_MESSAGE = "Good game though.";

    private static final long NOW = System.currentTimeMillis();
    private static final long FIRST_MESSAGE_TIME = NOW - TimeUnit.SECONDS.toMillis(2);
    private static final long SECOND_MESSAGE_TIME = NOW;

    @Mock
    Clock clock;

    private MessageRepository messageRepository;

    @Before
    public void setUp() {
        messageRepository = new MessageRepository(clock);

        BDDMockito
            .given(clock.now())
            .willReturn(FIRST_MESSAGE_TIME, SECOND_MESSAGE_TIME);
    }

    @Test public void
    add_a_message_when_user_post_message() {
        messageRepository.addMessageForUser(BOB, BOB_FIRST_MESSAGE);

        Assertions
            .assertThat(messageRepository.getMessagesForUser(BOB))
            .containsSequence(new Message(BOB_FIRST_MESSAGE, FIRST_MESSAGE_TIME));
    }

    @Test public void
    return_user_messages_when_requested_by_any_user() {
        messageRepository.addMessageForUser(BOB, BOB_FIRST_MESSAGE);
        messageRepository.addMessageForUser(BOB, BOB_SECOND_MESSAGE);

        Assertions
            .assertThat(messageRepository.getMessagesForUser(BOB))
            .containsSequence(
                new Message(BOB_SECOND_MESSAGE, SECOND_MESSAGE_TIME),
                new Message(BOB_FIRST_MESSAGE, FIRST_MESSAGE_TIME)
            );
    }
}
