package social;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;

@RunWith(MockitoJUnitRunner.class)
public class ViewUserWallCommandShould {

    private static final long NOW = System.currentTimeMillis();
    private static final String ALICE = "Alice";
    private static final Message ALICE_MESSAGE = new Message("I love the weather today", NOW - TimeUnit.MINUTES.toMillis(5));
    private static final String BOB = "Bob";
    private static final Message BOB_MESSAGE_ONE = new Message("Damn! We lost!", NOW - TimeUnit.MINUTES.toMillis(2));
    private static final Message BOB_MESSAGE_TWO = new Message("Good game though.", NOW - TimeUnit.MINUTES.toMillis(6));
    private static final String CHARLIE = "Charlie";
    private static final Message CHARLIE_MESSAGE = new Message("I'm in New York today! Anyone wants to have a coffee?", NOW - TimeUnit.SECONDS.toMillis(15));

    @Mock
    MessageRepository messageRepository;
    @Mock
    UserRepository userRepository;
    @Mock
    Output output;
    @Mock
    Clock clock;

    private MessageFormatter messageFormatter;

    @Before
    public void setUp() {
        messageFormatter = new MessageFormatter();
    }

    @Test public void
    output_the_user_message_timeline() {
        BDDMockito
                .when(clock.now())
                .thenReturn(NOW);

        BDDMockito
                .when(userRepository.getUsersFollowedBy(ALICE))
                .thenReturn(new HashSet<>());

        BDDMockito
                .when(messageRepository.getMessagesForUser(ALICE))
                .thenReturn(Arrays.asList(ALICE_MESSAGE));

        new ViewUserWallCommand(userRepository, messageRepository, messageFormatter, output, clock, ALICE).execute();

        InOrder outputVerifier = Mockito.inOrder(output);
        verifyOutput(outputVerifier, output, "Alice - I love the weather today (5 minutes ago)");
    }

    @Test public void
    output_the_user_and_users_followed_message_timeline() {
        BDDMockito
            .when(clock.now())
            .thenReturn(NOW);

        BDDMockito
            .when(userRepository.getUsersFollowedBy(ALICE))
            .thenReturn(Arrays.asList(BOB, CHARLIE));

        BDDMockito
            .when(messageRepository.getMessagesForUser(ALICE))
            .thenReturn(Arrays.asList(ALICE_MESSAGE));

        BDDMockito
            .when(messageRepository.getMessagesForUser(BOB))
            .thenReturn(Arrays.asList(BOB_MESSAGE_ONE, BOB_MESSAGE_TWO));

        BDDMockito
            .when(messageRepository.getMessagesForUser(CHARLIE))
            .thenReturn(Arrays.asList(CHARLIE_MESSAGE));

        new ViewUserWallCommand(userRepository, messageRepository, messageFormatter, output, clock, ALICE).execute();

        InOrder outputVerifier = Mockito.inOrder(output);
        verifyOutput(outputVerifier, output, "Charlie - I'm in New York today! Anyone wants to have a coffee? (15 seconds ago)");
        verifyOutput(outputVerifier, output, "Bob - Damn! We lost! (2 minutes ago)");
        verifyOutput(outputVerifier, output, "Alice - I love the weather today (5 minutes ago)");
        verifyOutput(outputVerifier, output, "Bob - Good game though. (6 minutes ago)");
    }

    @Test public void
    output_user_not_found_message_if_the_user_does_not_exists() {
        BDDMockito
            .willThrow(new UserNotFoundException(ALICE))
            .given(userRepository)
            .getUsersFollowedBy(ALICE);

        new ViewUserWallCommand(userRepository, messageRepository, messageFormatter, output, clock, ALICE).execute();

        Mockito
            .verify(output, Mockito.times(1))
            .printLine("User not found: Alice");
    }

    private void verifyOutput(InOrder outputVerifier, Output output, String line) {
        outputVerifier
                .verify(output, Mockito.times(1))
                .printLine(line);
    }
}
