package social;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class FollowUserCommandShould {

    private static final String BOB = "Bob";
    private static final String ALICE = "Alice";

    @Mock
    UserRepository userRepository;
    @Mock
    Output output;

    @Test public void
    add_the_target_user_to_the_followed_users() {
        new FollowUserCommand(userRepository, output, ALICE, BOB).execute();

        Mockito
            .verify(userRepository, Mockito.times(1))
            .followUser(ALICE, BOB);
    }

    @Test public void
    output_user_not_found_message_if_the_user_does_not_exists() {
        BDDMockito
            .willThrow(new UserNotFoundException(ALICE))
            .given(userRepository)
            .followUser(ALICE, BOB);

        new FollowUserCommand(userRepository, output, ALICE, BOB).execute();

        Mockito
            .verify(output, Mockito.times(1))
            .printLine("User not found: Alice");
    }
}
