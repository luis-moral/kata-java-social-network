package social;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PostMessageCommandShould {

    private static final String ALICE = "Alice";
    private static final String ALICE_MESSAGE = "I love the weather today";

    @Mock
    MessageRepository messageRepository;
    @Mock
    UserRepository userRepository;

    @Test public void
    create_a_new_user_and_post_message_if_user_does_not_exists() {
        new PostMessageCommand(ALICE, ALICE_MESSAGE, userRepository, messageRepository).execute();

        Mockito
            .verify(userRepository, Mockito.times(1))
            .create(ALICE);

        Mockito
            .verify(messageRepository, Mockito.times(1))
            .addMessageForUser(ALICE, ALICE_MESSAGE);
    }
}
