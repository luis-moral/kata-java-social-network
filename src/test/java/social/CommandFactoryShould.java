package social;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

public class CommandFactoryShould {

    private static final String POST_MESSAGE_COMMAND = "Alice -> I love the weather today";
    private static final String READ_MESSAGE_COMMAND = "Alice";
    private static final String FOLLOW_USER_COMMAND = "Alice follows Bob";
    private static final String VIEW_USER_WALL_COMMAND = "Alice wall";

    @Mock
    UserRepository userRepository;

    @Mock
    MessageRepository messageRepository;
    @Mock
    Output output;
    @Mock
    MessageFormatter messageFormatter;
    @Mock
    Clock clock;

    private CommandFactory commandFactory;

    @Before
    public void setUp() {
        commandFactory = new CommandFactory(userRepository, messageRepository, output, messageFormatter, clock);
    }

    @Test public void
    create_post_message_commands() {
        Assertions
            .assertThat(commandFactory.create(POST_MESSAGE_COMMAND))
            .isEqualTo(new PostMessageCommand("Alice", "I love the weather today", userRepository, messageRepository));
    }

    @Test public void
    create_read_messages_commands() {
        Assertions
            .assertThat(commandFactory.create(READ_MESSAGE_COMMAND))
            .isEqualTo(new ReadMessagesCommand(messageFormatter, messageRepository, output, clock, "Alice"));
    }

    @Test public void
    create_follow_user_commands() {
        Assertions
            .assertThat(commandFactory.create(FOLLOW_USER_COMMAND))
            .isEqualTo(new FollowUserCommand(userRepository, output, "Alice", "Bob"));
    }

    @Test public void
    create_view_user_wall_commands() {
        Assertions
            .assertThat(commandFactory.create(VIEW_USER_WALL_COMMAND))
            .isEqualTo(new ViewUserWallCommand(userRepository, messageRepository, messageFormatter, output, clock, "Alice"));
    }

    @Test public void
    create_error_parsing_command_when_cannot_parse_a_command() {
        Assertions
            .assertThat(commandFactory.create("Alice -< Some message"))
            .isEqualTo(new ErrorParsingCommand(output));
    }
}