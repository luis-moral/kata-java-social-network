package social;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SocialNetworkApiShould {

    private static final String ALICE_POST_MESSAGE_COMMAND = "Alice -> I love the weather today";
    private static final String ALICE_READ_MESSAGES_COMMAND = "Alice";
    private static final String ALICE_FOLLOW_BOB_COMMAND = "Alice follows Bob";
    private static final String VIEW_ALICE_WALL_COMMAND = "Alice wall";

    @Mock
    MessageRepositoryShould messageRepository;
    @Mock
    CommandFactory commandFactory;

    private SocialNetworkApi socialNetworkApi;

    @Before
    public void setUp() {
        socialNetworkApi = new SocialNetworkApi(commandFactory);
    }

    @Test public void
    execute_post_message_command() {
        PostMessageCommand postMessageCommand = Mockito.mock(PostMessageCommand.class);

        BDDMockito
            .given(commandFactory.create(ALICE_POST_MESSAGE_COMMAND))
            .willReturn(postMessageCommand);

        socialNetworkApi.executeCommand(ALICE_POST_MESSAGE_COMMAND);

        Mockito
            .verify(postMessageCommand, Mockito.times(1))
            .execute();
    }

    @Test public void
    execute_read_user_messages_command() {
        ReadMessagesCommand readMessageCommand = Mockito.mock(ReadMessagesCommand.class);

        BDDMockito
            .given(commandFactory.create(ALICE_READ_MESSAGES_COMMAND))
            .willReturn(readMessageCommand);

        socialNetworkApi.executeCommand(ALICE_READ_MESSAGES_COMMAND);

        Mockito
            .verify(readMessageCommand, Mockito.times(1))
            .execute();
    }

    @Test public void
    execute_follow_user_command() {
        FollowUserCommand followUserCommand = Mockito.mock(FollowUserCommand.class);

        BDDMockito
            .given(commandFactory.create(ALICE_FOLLOW_BOB_COMMAND))
            .willReturn(followUserCommand);

        socialNetworkApi.executeCommand(ALICE_FOLLOW_BOB_COMMAND);

        Mockito
            .verify(followUserCommand, Mockito.times(1))
            .execute();
    }

    @Test public void
    execute_view_user_wall_command() {
        ViewUserWallCommand viewUserWallCommand = Mockito.mock(ViewUserWallCommand.class);

        BDDMockito
            .given(commandFactory.create(VIEW_ALICE_WALL_COMMAND))
            .willReturn(viewUserWallCommand);

        socialNetworkApi.executeCommand(VIEW_ALICE_WALL_COMMAND);

        Mockito
            .verify(viewUserWallCommand, Mockito.times(1))
            .execute();
    }
}
